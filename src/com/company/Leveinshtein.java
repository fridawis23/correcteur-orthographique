package com.company;

public class Leveinshtein {
    public int minimum(int val1, int val2, int val3){
        return Math.min(Math.min(val1, val2), val3);
    }


    public int distanceLevenshtein (String firstWord, String secondWord) {


        int firstWordLenght = firstWord.length();
        int secondWordLenght = secondWord.length();

        // pour pouvoir faire la comparaison entre de deux caract�re
        firstWord = firstWord.toUpperCase();
        secondWord = secondWord.toUpperCase();
        int d[][] = new int[firstWordLenght + 1][secondWordLenght + 1];

        int cout;

        for (int i = 0; i < firstWordLenght + 1; i++) {
            d[i][0] = i;
        }
        for (int i = 0; i < secondWordLenght + 1; i++) {
            d[0][i] = i;
        }
        for (int i = 1; i < firstWordLenght + 1; i++) {
            for (int j = 1; j < secondWordLenght + 1; j++) {

                if (firstWord.charAt(i - 1) == secondWord.charAt(j - 1)) {
                    cout = 0;
                }
                else {
                    cout = 1;
                }
                d[i][j] = minimum(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + cout);

            }
        }
        return d[firstWordLenght][secondWordLenght];
    }


    public  Double d(String chaine1, String chaine2){

        Double distanceLevenshtein = (double) distanceLevenshtein(chaine1, chaine2);
        Double max = (double) Math.max(chaine1.length(), chaine2.length());

        return 1.0 - distanceLevenshtein /max;


    }
        }









