package com.company;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class TableMotTrigramme {
    Dictionnaire dictionnaire=new Dictionnaire();
    HashMap<String,List<String>> table=new HashMap<>();

    public TableMotTrigramme() throws IOException {
        table();
    }

    public HashMap<String, List<String>> getTable() {
        return table;
    }

    public  void table() throws IOException {
        for (String element : dictionnaire.getDictionnaire()) {
            table.put(element, new LinkedList<>());
        }
        for (String element : dictionnaire.getDictionnaire()) {
            String newWord = "<" + element.toLowerCase() + ">";
            String trig = "";
            for (int i = 0; i < newWord.length() - 2; i++) {
                trig += newWord.substring(i, i + 3);
                table.get(element).add(trig);
                trig="";
            }

        }

    }
}
