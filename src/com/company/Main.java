package com.company;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String [] args) throws IOException {
         Dictionnaire dictionnaire=new Dictionnaire();


        List<String> listeInitial=new LinkedList<>();
        File file=new File("fautes.txt");
          Scanner sc =new Scanner(file);
        while (sc.hasNextLine()) {
            String s = sc.nextLine();
            listeInitial.add(s);
        }
        sc.close();

        List<String> listeFinal = new LinkedList<>();
        System.out.println(listeInitial);
        double timeDebut=System.nanoTime();
        for(int i=0;i<5;i++){
            if (dictionnaire.getDictionnaire().contains(listeInitial.get(i))){
                listeFinal.add(listeInitial.get(i));

            }
            listeFinal.add(new Correcteur(listeInitial.get(i)).motCorriger());
        }
        System.out.println(listeFinal);
        double timeFin=System.nanoTime()-timeDebut;
        System.out.println("le temps d'execution est de: "+timeFin);


    }
}
