package com.company;

import java.io.IOException;
import java.util.*;

public class Correcteur {
    String word;
    Dictionnaire dictionnaire;
    List<String> list5;
    List<String> listeMotTrigrammeCommun;
    List<String> listeMotProche ;


    public Correcteur(String word) throws IOException {
        this.word=word;
        this.list5=new LinkedList<>();
        this.dictionnaire=new Dictionnaire();
        this.listeMotTrigrammeCommun =new ArrayList<>();
        this.listeMotProche =new ArrayList<>();



    }


    public List trigrammesencommun() throws IOException {
        TableMotTrigramme table =new TableMotTrigramme();
        Trigrammes trigrammes=new Trigrammes(word);

        for(int i=0;i<dictionnaire.getDictionnaire().size();i++){
            for(String element:trigrammes.listeTrigramme){
                  if(table.getTable().get(dictionnaire.getDictionnaire().get(i)).contains(element)){
                      listeMotTrigrammeCommun.add(dictionnaire.getDictionnaire().get(i));
                  }
                }
            }
        return listeMotTrigrammeCommun;

    }



    public int nbApparition(String word) throws IOException {
        int nb=0;
        nb= Collections.frequency(listeMotTrigrammeCommun,word);
        return nb;
    }


    public List<String> motProche() throws IOException {
            this.trigrammesencommun();
            int nb=(new Trigrammes(word).listeTrigramme.size());
        for(int i=nb;i>0;i--){
            for(String elementL: listeMotTrigrammeCommun) {
                if (!listeMotProche.contains(elementL) && nbApparition(elementL) == i) {
                    if (listeMotProche.size() < 100) {
                        listeMotProche.add(elementL);
                    }

                }

            }
        }
        return listeMotProche;
    }



    public void corriger() throws IOException {
        motProche();
        Leveinshtein leveinshtein=new Leveinshtein();
        for (int distanceEdition=1;distanceEdition<10;distanceEdition++){
          for(int i = 0; i< listeMotProche.size(); i++) {
              if (list5.size() == 5) {
                  break;}
              if (leveinshtein.distanceLevenshtein(word, listeMotProche.get(i)) ==distanceEdition && !list5.contains(listeMotProche.get(i))) {

                      list5.add(listeMotProche.get(i));

                  }
                  }
              }
    }




    public String motCorriger() throws IOException {
        corriger();
        if (list5.size()!=0){
            return list5.get(0);
        }
        return word;

    }
}
