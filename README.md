# Correcteur-Orthographique

Mon programme sur le correcteur orthographique comporte six classes 

# La Classe Dictionnaire   

Lit et stock le fichier dico.txt dans une liste  
 
# La Classe TableTrigramme 

Pour chaque mot du dictionnaire est associé sa liste de trigramme stocke dans un  Hasmap avec clé 
les mots du dictionnaire et valeur leur liste de trigramme stocké Chaque mot avec 
 
# La Classe Trigrammes 

Construit la liste de trigramme d’un mot. 
 
# La classe Levenshtein 

Calcul la distance d'Edition entre deux mots. 
 
# La Classe Correcteur 

Cette permet de corriger un mot non orthographier 
1- Récupère dans une liste tous les mots ayant un trigramme en commun avec le mot corriger 
2- Dans une liste de taille limiter à 100, récupère les mots qui ont le plus de trigramme en 
commun avec le mot à corriger 
3- Dans cette partie dans une liste de taille limité à 5 je rempli cette liste avec les mots ayant la 
plus distance d'Edition avec le mot à corriger  
4- le mot corriger est le premier élément de la liste limité à 5 sinon si cette liste vide c’est qu’il 
n’y a pas de correction possible pour ce mot 
 
# La Classe Main 
Dans cette classe je récupère les mots de du fichier Fautes.txt dans une liste “listeInitial’ et pour 
chaque mot de cette de cette liste on vérifie si le mot n'existe pas dans le dictionnaire sinon on 
procède à la correction du mot. 
Les mots corrigés sont stockés dans une liste “liste Final” 
 
Le programme met un peu de temps pour retourner la liste de mot corriger. 
